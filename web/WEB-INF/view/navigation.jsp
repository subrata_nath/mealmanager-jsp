<%--
  Created by IntelliJ IDEA.
  User: subrata
  Date: 11/29/16
  Time: 1:32 PM
  To change this template use File | Settings | File Templates.
--%>
<ul>

    <c:set var="active" value="0" scope="page"/>

    <c:choose>
        <c:when test="${not empty param.viewitems}">
            <c:set var="active" value="1" scope="page"/>
            <li><a class="active" href="mainmenu?viewitems=true">View Items</a></li>
        </c:when>
        <c:otherwise>
            <li><a href="mainmenu?viewitems=true">View Items</a></li>
        </c:otherwise>
    </c:choose>


    <c:choose>
        <c:when test="${not empty param.viewmealplan}">
            <c:set var="active" value="1" scope="page"/>
            <li><a class="active" href="mainmenu?viewmealplan=true">View MealPlan</a></li>
        </c:when>
        <c:otherwise>
            <li><a href="mainmenu?viewmealplan=true">View MealPlan</a></li>
        </c:otherwise>
    </c:choose>


    <c:choose>
        <c:when test="${not empty param.editmealplan}">
            <c:set var="active" value="1" scope="page"/>
            <li><a class="active" href="mainmenu?selectmeal=true">Edit MealPlan</a></li>
        </c:when>
        <c:otherwise>
            <li><a href="mainmenu?selectmeal=true">Edit MealPlan</a></li>
        </c:otherwise>
    </c:choose>

    <li><a href="mainmenu?logout=true">Log Out</a></li>
</ul>

