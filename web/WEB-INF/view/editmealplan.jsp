<%--
  Created by IntelliJ IDEA.
  User: subrata
  Date: 11/30/16
  Time: 2:14 PM
  To change this template use File | Settings | File Templates.
--%>
<div class="col-md-8">
    <form action="mainmenu" method="post">
        <input name="selectmeal" value="true" hidden>

        <table class="table">
            <caption><h2>Edit Meal Plans:</h2></caption>

            <tr>
                <th>Day</th>
                <th>Meal Type</th>
                <th>Items</th>
            </tr>

            <c:set var="meal" value="${particularmeal}" scope="page"/>
            <tr>
                <td>
                    <label><c:out value="${meal.day}"/></label>
                </td>
                <td>
                    <label><c:out value="${meal.type}"/></label>
                </td>
                <td>
                    <input name="mealid" value="${meal.id}" hidden>
                    <c:forEach items="${meal.items}" var="item">
                        <label>
                            <input checked type="checkbox" name="item=${item.name}"/>
                            <c:out value="${item.name}"/><br>
                        </label>
                    </c:forEach>

                    <c:forEach items="${suggestitems}" var="item">
                        <label>
                            <input type="checkbox" name="item=${item.name}"/>
                            <c:out value="${item.name}"/><br>
                        </label>
                    </c:forEach><br>

                    <button type="submit" class="btn btn-primary">Submit Changes</button>
                </td>

            </tr>
        </table>
    </form>
</div>
