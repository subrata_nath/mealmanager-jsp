<%--
  Created by IntelliJ IDEA.
  User: subrata
  Date: 11/29/16
  Time: 12:33 PM
  To change this template use File | Settings | File Templates.
--%>

<div class="container">
    <div class="col-md-6">

        <form action="mainmenu" method="post">

            <input name="delete" value="true" hidden>

            <table class="table">
                <h3>Available Food Items:</h3>
                <tr>
                    <th>Delete?</th>
                    <th>#</th>
                    <th>Item</th>
                </tr>

                <c:set var="itemcount" value="0" scope="page"/>

                <c:forEach items="${items}" var="item">
                    <tr>
                        <c:set var="itemcount" value="${itemcount + 1}" scope="page"/>

                        <td><input type="checkbox" name="delete=<c:out value="${item.name}"/>"></td>
                        <td><c:out value="${itemcount}"/></td>
                        <td><c:out value="${item.name}"/></td>
                    </tr>
                </c:forEach>

            </table>

            <input name="itemcount" value="<c:out value="${itemcount}"/>" hidden>
            <button type="submit" class="btn btn-primary">Update Items</button>
        </form>
        <br>
    </div>

    <div class="col-md-6">
        <c:choose>

            <c:when test="${not empty param.insertitems}">
                <%@include file="/WEB-INF/view/insertnewitems.jsp" %>
            </c:when>

            <c:otherwise>
                <form action="mainmenu" method="post">

                    <input name="insertitems" value="true" hidden>

                    <h3>Want to Insert Items?</h3>
                    <h5>How Many? (max=20)</h5>
                    <input type="number" min="1" max="20" name="rows" value="1" required>

                    <button type="submit" class="btn btn-primary">Insert New Items</button>
                </form>
            </c:otherwise>
        </c:choose>

    </div>
</div>
