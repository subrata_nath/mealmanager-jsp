<%--
  Created by IntelliJ IDEA.
  User: subrata
  Date: 11/29/16
  Time: 1:32 PM
  To change this template use File | Settings | File Templates.
--%>
<div class="container">
    <div class="col-md-12">
        <div class="col-md-6">
            <table class="table">
                <caption><h3>Available Day List To Serve Meal:</h3></caption>
                <tr>
                    <th>#</th>
                    <th>Item</th>
                </tr>

                <c:set var="count" value="0" scope="page"/>

                <c:forEach items="${days}" var="day">
                    <c:set var="count" value="${count + 1}" scope="page"/>

                    <tr>
                        <td><c:out value="${count}"/></td>
                        <td><c:out value="${day}"/></td>
                    </tr>
                </c:forEach>
            </table>
        </div>

        <div class="col-md-6">
            <table class="table">
                <caption><h3>Available Meal types:</h3></caption>

                <tr>
                    <th>#</th>
                    <th>Meal Type</th>
                </tr>

                <c:set var="count" value="0" scope="page"/>

                <c:forEach items="${types}" var="type">
                    <c:set var="count" value="${count + 1}" scope="page"/>

                    <tr>
                        <td><c:out value="${count}"/></td>
                        <td><c:out value="${type}"/></td>
                    </tr>
                </c:forEach>
            </table>
        </div>

        <div class="col-md-8">
            <table class="table">
                <caption><h2>Available Meal Plans:</h2></caption>

                <tr>
                    <th>Day</th>
                    <th>Meal Type</th>
                    <th>Items</th>
                    <th>Option</th>
                </tr>

                <c:forEach items="${mealplans}" var="meal">
                    <tr>
                        <td>
                            <label><c:out value="${meal.day}"/></label>
                        </td>
                        <td>
                            <label><c:out value="${meal.type}"/></label>
                        </td>
                        <td>
                            <c:forEach items="${meal.items}" var="item">
                                <label><c:out value="${item.name}"/></label>
                            </c:forEach>
                        </td>
                        <td>
                            <form action="mainmenu" method="post">
                                <input name="editmeal" value="true" hidden>
                                <input name="mealid=${meal.id}" value="Update" type="submit">
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>

    </div>
</div>