<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: subrata
  Date: 12/1/2016
  Time: 12:49 AM
  To change this template use File | Settings | File Templates.
--%>
<div class="col-md-4"></div>
<div class="col-md-4">

    <h3>To Add New Meal Plan or Edit Existing Plan Select:<br></h3>
    <div class="col-md-4">
        <form action="mealmenu" method="post">
            <input name="selectmeal" value="true" hidden>

            <h3>Day </h3>
            <select name="day">

                <label><h1> Select Meal Type To Edit MealPlan</h1></label>

                <c:forEach items="${days}" var="day">
                    <option value="${day}"><c:out value="${day}"/></option>
                </c:forEach>

            </select>

            <h3>Meal Type</h3>
            <select name="type">

                <c:forEach items="${types}" var="type">
                    <option value="${type}"><c:out value="${type}"/></option>
                </c:forEach>

            </select>

            <br><br>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
</div>
