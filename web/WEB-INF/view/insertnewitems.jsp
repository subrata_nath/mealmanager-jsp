<%--
  Created by IntelliJ IDEA.
  User: subrata
  Date: 11/29/16
  Time: 4:25 PM
  To change this template use File | Settings | File Templates.
--%>
<form action="mainmenu" method="post">

    <input name="insert" value="true" hidden="">

    <table class="table">
        <h3>Insert New Food Item Names:</h3>

        <tr>
            <th>#</th>
            <th>Item Name</th>
        </tr>

        <c:set var="itemcount" value="0" scope="page"/>

        <c:forEach var="i" begin="1" end="${param.rows}">

            <c:set var="itemcount" value="${itemcount + 1}" scope="page"/>

            <tr>
                <td><c:out value="${itemcount}"/></td>
                <td><input name="newitem<c:out value="${itemcount}"/>" type="text"></td>
            </tr>
        </c:forEach>
    </table>

    <input name="itemcount" value="<c:out value="${itemcount}"/>" hidden>
    <button type="submit" class="btn btn-primary">Submit Items</button>
</form>