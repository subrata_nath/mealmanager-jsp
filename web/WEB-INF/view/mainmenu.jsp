<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: subrata
  Date: 11/27/16
  Time: 2:04 PM
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>
    <style type="text/css">
        <%@include file="/resources/style/css/table.scss" %>
        <%@include file="/resources/style/css/mystyle.css" %>
    </style>

    <title>Welcome Home</title>
</head>

<body>

<%@include file="/WEB-INF/view/navigation.jsp" %>

<c:choose>
    <c:when test="${not empty param.viewitems}">
        <%@include file="/WEB-INF/view/viewitems.jsp" %>
    </c:when>
    <c:when test="${not empty param.viewmealplan}">
        <%@include file="/WEB-INF/view/mealplan.jsp" %>
    </c:when>
    <c:when test="${not empty param.editmeal}">
        <%@include file="/WEB-INF/view/editmealplan.jsp" %>
    </c:when>
    <c:when test="${not empty param.selectmeal}">
        <%@include file="/WEB-INF/view/selectmeal.jsp" %>
    </c:when>
    <c:otherwise>
        <%@include file="/WEB-INF/view/viewitems.jsp" %>
    </c:otherwise>
</c:choose>

</body>
</html>
