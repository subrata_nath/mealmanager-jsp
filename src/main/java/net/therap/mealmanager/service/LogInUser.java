package net.therap.mealmanager.service;

import net.therap.mealmanager.dao.UserDao;
import net.therap.mealmanager.model.User;

/**
 * @author subrata
 * @since 11/28/16
 */
public class LogInUser {

    public boolean getUser(String email, String password) {

        UserDao userDao = new UserDao();

        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
        user.setAdmin(0);

        return userDao.isUserExists(user);
    }
}
