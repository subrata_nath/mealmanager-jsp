package net.therap.mealmanager.service;

import net.therap.mealmanager.dao.ItemDao;
import net.therap.mealmanager.dao.MealDao;
import net.therap.mealmanager.model.Day;
import net.therap.mealmanager.model.Item;
import net.therap.mealmanager.model.Meal;
import net.therap.mealmanager.model.Type;

import java.util.ArrayList;
import java.util.List;

/**
 * @author subrata
 * @since 11/15/16
 */
public class MealManagementService {

    public Day[] getDays() {
        return Day.values();
    }

    public Type[] getTypes() {
        return Type.values();
    }

    public List<Meal> showAllMealPlans() {
        MealDao mealDao = new MealDao();
        return mealDao.getAllMealPlans();
    }

    public Meal getParticularMeal(int mealId) {
        MealDao mealDao = new MealDao();
        return mealDao.readParticularMeal(mealId);
    }

    public List<Item> getNotSelectedItems(Meal meal) {

        ItemDao itemDao = new ItemDao();

        List<Item> NotSelectedItems = new ArrayList<>();

        List<Item> allItems = itemDao.getItems();

        for (Item universalItem : allItems) {
            boolean selected = false;
            for (Item includedItem : meal.getItems()) {

                if (universalItem.getName().equals(includedItem.getName())) {
                    selected = true;
                }
            }

            if (!selected) {
                NotSelectedItems.add(universalItem);
            }

        }

        return NotSelectedItems;
    }
}
