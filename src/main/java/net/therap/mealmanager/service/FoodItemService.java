package net.therap.mealmanager.service;

import net.therap.mealmanager.dao.ItemDao;
import net.therap.mealmanager.dao.MealDao;
import net.therap.mealmanager.model.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * @author subrata
 * @since 11/15/16
 */
public class FoodItemService {


    public List<Item> getAllItems() {

        ItemDao itemDao = new ItemDao();
        List<Item> items = itemDao.getItems();

        return items;
    }

    public void deleteItems(List<String> itemNames) {
        ItemDao itemDao = new ItemDao();
        List<Item> items = new ArrayList<>();

        for (String itemName : itemNames) {
            Item item = new Item();
            item.setName(itemName);
            items.add(item);
        }

        itemDao.deleteItemsByName(items);
    }

    public void insertItems(List<String> itemNames) {
        ItemDao itemDao = new ItemDao();
        List<Item> items = itemDao.getItems();

        for (String itemName : itemNames) {

            Item item = new Item();
            item.setName(itemName);
            items.add(item);
        }

        itemDao.addItems(items);
    }

    public void updateMeal(int mealId, List<String> itemNames) {

        MealDao mealDao=new MealDao();
        mealDao.updateMeal(mealId,itemNames);
    }

    public int getMealIdFromDayAndType(String day, String type) {
        MealDao mealDao=new MealDao();
        return mealDao.getMealId(day,type);
    }
}
