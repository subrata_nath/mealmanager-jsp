package net.therap.mealmanager.utils;

import net.therap.mealmanager.model.Day;
import net.therap.mealmanager.model.Type;

/**
 * @author subrata
 * @since 11/22/16
 */
public class StringToEnum {

    public static Day dayNameToDayEnum(String day) {

        day = day.toLowerCase();

        switch (day) {

            case "sunday":
                return Day.SUNDAY;

            case "monday":
                return Day.MONDAY;

            case "tuesday":
                return Day.TUESDAY;

            case "wednesday":
                return Day.WEDNESDAY;

            case "thursday":
                return Day.THURSDAY;

            default:
                return null;
        }
    }

    public static Type typeNameToTypeEnum(String type) {

        type = type.toLowerCase();

        switch (type) {

            case "lunch":
                return Type.LUNCH;

            case "breakfast":
                return Type.BREAKFAST;

            default:
                return null;
        }
    }
}
