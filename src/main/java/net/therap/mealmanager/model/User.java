package net.therap.mealmanager.model;

import javax.persistence.*;

/**
 * @author subrata
 * @since 11/28/16
 */

@Entity
@Table(name = "user")
public class User {
    @Id
    @Column(name = "email")
    private String email;

    @Column(name = "password", length = 100, nullable = false)
    private String password;


    @Column(name = "admin",nullable = false)
    private int admin=0;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAdmin() {
        return admin;
    }

    public void setAdmin(int admin) {
        this.admin = admin;
    }
}
