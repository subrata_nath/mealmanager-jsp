package net.therap.mealmanager.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * @author subrata
 * @since 11/22/16
 */
@Entity
@Table(name = "meal", uniqueConstraints = {@UniqueConstraint(columnNames = {"day", "type"})})
public class Meal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "day", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private Day day;

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private Type type;

    @ManyToMany
    @JoinTable(
            name = "meal_item",
            joinColumns = @JoinColumn(name = "meal_id"),
            inverseJoinColumns = @JoinColumn(name = "item_id")
    )
    private Set<Item> items = new HashSet<>();

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Set<Item> getItems() {
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
