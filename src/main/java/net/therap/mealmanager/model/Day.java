package net.therap.mealmanager.model;

/**
 * @author subrata
 * @since 11/22/16
 */
public enum Day {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY
}

