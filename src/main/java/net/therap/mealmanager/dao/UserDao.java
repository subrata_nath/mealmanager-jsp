package net.therap.mealmanager.dao;

import net.therap.mealmanager.model.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 * @author subrata
 * @since 11/28/16
 */
public class UserDao extends GenericDao<User> {

    public UserDao() {
        classType = User.class;
    }

    public boolean addUser(User user) {

        if (!isUserExists(user)) {

            insertSingleEntity(user);
            return true;

        } else {
            return false;
        }
    }

    public boolean isUserExists(User user) {

        Session session = getCurrentSession();
        Transaction transaction = session.beginTransaction();

        user = (User) session.createCriteria(User.class)
                .add(Restrictions.eq("email", user.getEmail()))
                .add(Restrictions.eq("password", user.getPassword()))
                .add(Restrictions.eq("admin", 0))
                .uniqueResult();

        transaction.commit();

        if (user != null) {
            return true;
        } else {
            return false;
        }
    }
}
