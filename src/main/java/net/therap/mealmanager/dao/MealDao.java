package net.therap.mealmanager.dao;

import net.therap.mealmanager.model.Day;
import net.therap.mealmanager.model.Item;
import net.therap.mealmanager.model.Meal;
import net.therap.mealmanager.model.Type;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.util.*;

/**
 * @author subrata
 * @since 11/22/16
 */
public class MealDao extends GenericDao<Meal> {

    public MealDao() {
        classType = Meal.class;
    }

    public List<Meal> getAllMealPlans() {
        Session session = getCurrentSession();
        Transaction transaction = session.beginTransaction();

        List<Meal> mealPlans = new ArrayList<>();

        List<Meal> meals = session.createCriteria(Meal.class).list();

        for (Meal meal : meals) {
            Set<Item> items = new HashSet<>();

            for (Item item : meal.getItems()) {
                items.add(item);
            }

            meal.setItems(items);
            mealPlans.add(meal);
        }

        transaction.commit();
        return mealPlans;
    }

    public Meal readParticularMeal(int mealId) {

        Session session = getCurrentSession();
        Transaction transaction = session.beginTransaction();

        Meal meal = (Meal) session.createCriteria(Meal.class)
                .add(Restrictions.eq("id", mealId)).uniqueResult();

        Set<Item> items = new HashSet<>();

        for (Item item : meal.getItems()) {
            items.add(item);
        }

        meal.setItems(items);
        transaction.commit();

        return meal;
    }

    public void updateMeal(int id, List<String> itemNames) {

        deleteMealById(id);

        Session session = getCurrentSession();
        Transaction transaction = session.beginTransaction();

        Meal meal = (Meal) session.createCriteria(Meal.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();

        Set<Item> items = new HashSet<>();

        for (String itemName : itemNames) {
            Item item = (Item) session.createCriteria(Item.class)
                    .add(Restrictions.eq("name", itemName)).uniqueResult();
            if (item != null) {
                items.add(item);
            }
        }
        meal.setItems(items);
        session.save(meal);
        transaction.commit();

    }

    public void deleteMealById(int id) {
        Session session = getCurrentSession();
        Transaction transaction = session.beginTransaction();

        Meal meal = (Meal) session.createCriteria(Meal.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();

        Set<Item> items = new HashSet<>();

        for (Item item : meal.getItems()) {
            items.add(item);
        }

        items.clear();
        meal.setItems(items);
        session.update(meal);
        transaction.commit();
    }

    public int getMealId(String day, String type) {
        Session session = getCurrentSession();
        Transaction transaction = session.beginTransaction();

        Meal meal = (Meal) session.createCriteria(Meal.class)
                .add(Restrictions.eq("type", type))
                .add(Restrictions.eq("day", day))
                .uniqueResult();

        transaction.commit();
        return meal.getId();
    }
}
