package net.therap.mealmanager.dao;

import net.therap.mealmanager.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

/**
 * @author subrata
 * @since 11/22/16
 */
public abstract class GenericDao<T> {

    public Class<T> classType;

    public Session getCurrentSession() {
        return HibernateUtil.getSessionFactory().getCurrentSession();
    }

    public List<T> readTable() {

        Session session = getCurrentSession();
        Transaction transaction = session.beginTransaction();

        List<T> entityList = session.createCriteria(classType).list();

        transaction.commit();

        return entityList;
    }

    public void insertEntityList(List<T> targetEntity) {

        Session session = getCurrentSession();
        Transaction transaction = session.beginTransaction();

        for (T target : targetEntity) {
            session.save(target);
        }

        transaction.commit();
    }

    public void deleteEntityList(List<T> targetEntity) {

        Session session = getCurrentSession();
        Transaction transaction = session.beginTransaction();

        for (T target : targetEntity) {
            session.delete(target);
        }

        transaction.commit();
    }

    public void insertSingleEntity(T entity) {

        Session session = getCurrentSession();
        Transaction transaction = session.beginTransaction();

        session.save(entity);

        transaction.commit();
    }

    public void deleteSingleEntity(T entity) {

        Session session = getCurrentSession();
        Transaction transaction = session.beginTransaction();

        session.delete(entity);

        transaction.commit();
    }

}
