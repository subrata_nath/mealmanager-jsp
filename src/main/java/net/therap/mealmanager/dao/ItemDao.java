package net.therap.mealmanager.dao;

import net.therap.mealmanager.model.Item;
import net.therap.mealmanager.model.Meal;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author subrata
 * @since 11/22/16
 */
public class ItemDao extends GenericDao<Item> {

    public ItemDao() {
        classType = Item.class;
    }

    public void addItems(List<Item> items) {

        for (Item item : items) {

            if (getItemIfExists(item) == null) {
                insertSingleEntity(item);
            }
        }
    }

    public void deleteItemsByName(List<Item> items) {

        for (Item item : items) {

            item = getItemIfExists(item);
            if (item != null) {
                deleteSingleEntity(item);
            }
        }
    }

    public List<Item> getItems() {

        List<Item> items = readTable();
        return items;
    }

    public Item getItemIfExists(Item item) {

        Session session = getCurrentSession();
        Transaction transaction = session.beginTransaction();

        item = (Item) session.createCriteria(Item.class)
                .add(Restrictions.eq("name", item.getName())).uniqueResult();

        transaction.commit();

        if (item != null) {
            return item;
        } else {
            return null;
        }
    }
}
