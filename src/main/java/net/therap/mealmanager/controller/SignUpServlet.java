package net.therap.mealmanager.controller;

import net.therap.mealmanager.service.SignUpUser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author subrata
 * @since 11/28/16
 */
@WebServlet(urlPatterns = {"/signup"})
public class SignUpServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();

        try {
            if (session != null && session.getAttribute("authenticated").equals("1")) {
                response.sendRedirect("/mealmanager/mainmenu");
            } else {
                request.getRequestDispatcher("/WEB-INF/view/signup.jsp").forward(request, response);
            }
        } catch (Exception e) {
            request.getRequestDispatcher("/WEB-INF/view/signup.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();

        String email = request.getParameter("email");
        String password = request.getParameter("password");


        if (email.length() > 0 && password.length() > 0) {

            SignUpUser signUpUser = new SignUpUser();

            if (signUpUser.addUser(email, password)) {

                session.setAttribute("email", email);
                session.setAttribute("password", password);
                session.setAttribute("authenticated", "1");

                response.sendRedirect("/mealmanager/mainmenu");

            } else {
                request.getRequestDispatcher("/WEB-INF/view/signup.jsp").forward(request, response);
            }

        }
    }
}
