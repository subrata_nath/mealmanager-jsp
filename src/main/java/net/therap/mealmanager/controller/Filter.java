package net.therap.mealmanager.controller;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author subrata
 * @since 11/27/16
 */
@WebFilter(urlPatterns = {"/mainmenu"})
public class Filter implements javax.servlet.Filter {

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException,
            IOException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        HttpSession session = request.getSession();

        if (request.getParameter("logout") != null && session.getAttribute("authenticated") != null) {
            session.removeAttribute("authenticated");
            session.removeAttribute("email");
            session.removeAttribute("password");
        }

        if (session == null) {
            response.sendRedirect("/mealmanager/login");

        } else if (session.getAttribute("authenticated") != null && session.getAttribute("authenticated").equals("1")) {
            chain.doFilter(req, resp);
        } else {
            response.sendRedirect("/mealmanager/login");
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
