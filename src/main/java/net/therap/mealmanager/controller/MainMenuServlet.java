package net.therap.mealmanager.controller;

import net.therap.mealmanager.model.Day;
import net.therap.mealmanager.model.Item;
import net.therap.mealmanager.model.Meal;
import net.therap.mealmanager.model.Type;
import net.therap.mealmanager.service.FoodItemService;
import net.therap.mealmanager.service.MealManagementService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author subrata
 * @since 11/27/16
 */
@WebServlet("/mainmenu")
public class MainMenuServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        preCalculate(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        manageOperationRequest(request);
        preCalculate(request, response);
    }

    private void preCalculate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        FoodItemService foodItemService = new FoodItemService();
        List<Item> items = foodItemService.getAllItems();

        MealManagementService mealManagementService = new MealManagementService();
        Day[] days = mealManagementService.getDays();
        Type[] types = mealManagementService.getTypes();

        List<Meal> mealPlans = mealManagementService.showAllMealPlans();

        request.setAttribute("items", items);
        request.setAttribute("days", days);
        request.setAttribute("types", types);
        request.setAttribute("mealplans", mealPlans);

        if (request.getParameter("editmeal") != null) {
            getParticularMealInfo(request);
        }

        request.getRequestDispatcher("/WEB-INF/view/mainmenu.jsp").forward(request, response);
    }

    private void getParticularMealInfo(HttpServletRequest request) {
        Set<String> parameters = request.getParameterMap().keySet();

        for (String parameter : parameters) {
            if (parameter.toLowerCase().startsWith("mealid=")) {
                int mealId = Integer.parseInt(parameter.substring(7));
                MealManagementService mealManagementService = new MealManagementService();
                Meal meal = mealManagementService.getParticularMeal(mealId);
                request.setAttribute("particularmeal", meal);
                request.setAttribute("suggestitems", mealManagementService.getNotSelectedItems(meal));
                break;
            }
        }
    }

    private void getParticularMealInfoFromDayAndType(HttpServletRequest request) {
        FoodItemService foodItemService = new FoodItemService();
        int mealId = foodItemService.getMealIdFromDayAndType(request.getParameter("day"),
                request.getParameter("type"));
        MealManagementService mealManagementService = new MealManagementService();
        Meal meal = mealManagementService.getParticularMeal(mealId);
        request.setAttribute("particularmeal", meal);
        request.setAttribute("suggestitems", mealManagementService.getNotSelectedItems(meal));

    }

    private void manageOperationRequest(HttpServletRequest request) {

        if (request.getParameter("delete") != null) {
            deleteItemRequest(request);
        } else if (request.getParameter("insert") != null) {
            insertItemRequest(request);
        } else if (request.getParameter("mealid") != null) {
            updateMeal(request);
        } else if (request.getParameter("selectmeal") != null
                && request.getParameter("day") != null && request.getParameter("type") != null) {
            System.out.println("ASD");
            getParticularMealInfoFromDayAndType(request);
        }
    }

    private void updateMeal(HttpServletRequest request) {
        FoodItemService foodItemService = new FoodItemService();

        List<String> itemNames = new ArrayList<>();

        Set<String> parameters = request.getParameterMap().keySet();

        for (String parameter : parameters) {
            if (parameter.toLowerCase().startsWith("item=")) {
                itemNames.add(parameter.substring(5));
            }
        }
        int mealId = Integer.parseInt(request.getParameter("mealid"));
        foodItemService.updateMeal(mealId, itemNames);
    }

    private void deleteItemRequest(HttpServletRequest request) {
        FoodItemService foodItemService = new FoodItemService();

        List<String> itemNames = new ArrayList<>();

        Set<String> parameters = request.getParameterMap().keySet();

        for (String parameter : parameters) {
            if (parameter.toLowerCase().startsWith("delete=")) {
                itemNames.add(parameter.substring(7));
            }
        }

        foodItemService.deleteItems(itemNames);
    }

    private void insertItemRequest(HttpServletRequest request) {
        FoodItemService foodItemService = new FoodItemService();

        List<String> itemNames = new ArrayList<>();

        for (int count = 1; count <= Integer.parseInt(request.getParameter("itemcount").toString()); count++) {

            String itemName = request.getParameter("newitem" + count).trim();

            if (itemName.length() > 0) {
                itemNames.add(itemName);
            }
        }

        foodItemService.insertItems(itemNames);
    }
}
